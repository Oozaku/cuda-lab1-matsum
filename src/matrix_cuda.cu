#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

__global__ void add(int *a, int *b, int *c, int *linhas, int *colunas){
    int l = *linhas, co = *colunas;
    long long unsigned ini = (long long unsigned) blockIdx.x * co;
    long long unsigned i, max = (long long unsigned)l*co;
    for (i = 0; i < co; i++){
        if (i+ini < max)
            c[i+ini] = a[i+ini] + b[i+ini];
    }
}

int main()
{
    int *A, *B, *C;
    int i, j;

    //Input
    int linhas, colunas;

    scanf("%d", &linhas);
    scanf("%d", &colunas);

    //Alocando memória na CPU
    A  = (int *)malloc(sizeof(int)*linhas*colunas);
    B  = (int *)malloc(sizeof(int)*linhas*colunas);
    C  = (int *)malloc(sizeof(int)*linhas*colunas);
    
    //Inicializar
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            A[i*colunas+j] =  B[i*colunas+j] = i+j;
        }
    }

    /*------------------------CUDA---------------------------*/

    int *da, *db, *dc, *dlinhas, *dcolunas;
    
    long long unsigned size = (long long unsigned) linhas * colunas * sizeof(int);
    
    cudaMalloc((void **)&da, size);    
    cudaMalloc((void **)&db, size);    
    cudaMalloc((void **)&dc, size);
    cudaMalloc((void **)&dlinhas, sizeof(int));
    cudaMalloc((void **)&dcolunas, sizeof(int));

    cudaMemcpy(da, A, size, cudaMemcpyHostToDevice);
    cudaMemcpy(db, B, size, cudaMemcpyHostToDevice);
    cudaMemcpy(dc, C, size, cudaMemcpyHostToDevice);
    cudaMemcpy(dlinhas, &linhas, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dcolunas, &colunas, sizeof(int), cudaMemcpyHostToDevice);
    
    add<<<linhas,1>>>(da,db,dc,dlinhas, dcolunas);

    cudaMemcpy(C, dc, size, cudaMemcpyDeviceToHost);
    
    cudaFree(da);
    cudaFree(db);
    cudaFree(dc);
    cudaFree(dlinhas);
    cudaFree(dcolunas);

    /*-------------------------------------------------------*/

    long long int somador=0;
    //Manter esta computação na CPU
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            somador+=C[i*colunas+j];   
            //printf("%i",C[i*colunas+j]);
        }
        //printf("\n");
    }
        
    printf("%lli\n", somador);

    free(A);
    free(B);
    free(C);
}

